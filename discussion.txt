Git VCS

What is Git VCS?

Git VCS is an open source version control system. It allows us to track and update our file with only the chamges made to it.

Gitlab vs Github

There is not much difference between the two. Both Gitlab and Github are just competing cloud services that store and manage our online repositories.

Repositories

Local Repository - folders that use got technology. Therefore, it allows us to track changes made in the files with the folder. These changes can then be uploaded and update the files in our Remote Repositories.


Remote Repositories - these are folders that use got technology but instead located in the internet or in cloud services such as Gitlab and Github.

What is an SSH Key?

SSh or Secure Shell Key are tools we use to authenticate the uploading or of other tasks when manipulating or using git repositories. It allows us to push/upload changes to our repos without the need for passwords.

Basic Git Commands:


git init - git init, allows us to initialize a folder as a local remote repository.

git add . - allows to track all the changes that we've made and prepare these files as a new version to be uploaded.

git commit -m "<commitMessage>" - allows us to create a new commit or version of our files to be pushed into our remote repositories.

git remote add <aliasOfRemote> <urlOfRemote> - allows us to add/connect a remote repository to our local repository.

git remote -v - allows us to view the remote repositories connected to our local repo.

git push <alias> master - allows us to push our updates/changes/version/commit/ into our remote repository.

git status - allows us to display files that are ready to be added and then comitted.

git clone - allows us to clone a remote repository and its contents.

git pull <remoteAlias> master - allows us to pull or get the updates from a remote repo to a local repo.

Git Config

These commands will allow us to have git recognize the person trying to push into an online/remote repo:

git config --global user.email "emailUsedInGitLab" - configure the email used to push into the remote repo.

git config --global user.name "userNameUsedInGitLab" - configure the name/username of the user trying to push into gitlab.

For pushing for the first time:

git init -> git remote add origin <urlOfRepo> -> git add . -> git commit -m "<commitMessage>" -> git push origin master.

For pushing with updates: 
git add . -> git commit -m "<commitMessage>" -> git push origin master.

Git remote -v and git status are used to check the remotes connected and the files ready to be added.